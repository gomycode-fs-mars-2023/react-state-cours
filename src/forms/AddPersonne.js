import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';

function AddPersonne() {
  const [validated, setValidated] = useState(false);
  const [firstName, setFirstName]= useState("Jeff");
  const [lastName, setLastName]= useState("...");
  const [userName, setUserName] = useState("ddd");
  const [personnes, setPersonnes] = useState([{firstName: "", lastName: "", userName: ""}])

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    
    setPersonnes([...personnes, {firstName, lastName, userName}]);

    console.log(personnes)
    setValidated(true);
  };

  return (
    <>
    <div>firstName : {firstName} </div>
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Row className="mb-3">
        <Form.Group as={Col} md="4" controlId="validationCustom01">
          <Form.Label>First name</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="First name"
            defaultValue={firstName}
            onChange={(e)=>setFirstName(e.target.value)}
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom02">
          <Form.Label>Last name</Form.Label>
          <Form.Control
            required
            type="email"
            placeholder="Last name"
            onChange={(e)=>setLastName(e.target.value)}
            defaultValue={lastName}
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustomUsername">
          <Form.Label>Username</Form.Label>
          <InputGroup hasValidation>
            <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
            <Form.Control
              type="text"
              placeholder="Username"
              aria-describedby="inputGroupPrepend"
              onChange={(e)=>setUserName(e.target.value)}
              defaultValue={userName}
              required
            />
            <Form.Control.Feedback type="invalid">
              Please choose a username.
            </Form.Control.Feedback>
          </InputGroup>
        </Form.Group>
      </Row>
      <Button type="submit">Soumettre</Button>
    </Form>
    </>
  );
}

export default AddPersonne;
