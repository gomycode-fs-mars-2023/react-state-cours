import logo from './logo.svg';
import './App.css';
import AddPersonne from "./forms/AddPersonne"
import Container from "react-bootstrap/Container";
function App() {
  return (
    <div className="App">
      <Container>
        <AddPersonne></AddPersonne>
      </Container> 
    </div>
  );
}

export default App;
